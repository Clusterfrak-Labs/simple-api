AWSTemplateFormatVersion: "2010-09-09"
# Description of what this CloudFormation Template is going to produce
Description: AWS CloudFormation Template to create Resources required to deploy the Jenkins-Deployment-Pipeline-Lab.
# StackName: Jenkins-Deployment-Pipeline-Lab-Resources

# ============================================================================
# NOTES: This template uses Exports produced by
# PreReq-Required-Templates:
#   - None
# ============================================================================

#===============================
# Resources:
# Define Resources that will be launched via this template
#===============================
Resources:
  # ----------------------------------
  # S3 Artifact Bucket
  # ----------------------------------
  JenkinsLabBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${AWS::AccountId}-jenkins-deployment-pipeline-lab-${AWS::Region}
      VersioningConfiguration:
        Status: Suspended

  # --------------------------------
  # ECR Repository
  # --------------------------------
  JenkinsLabECRRepository:
    Type: AWS::ECR::Repository
    Properties: 
      RepositoryName: simple_api
      RepositoryPolicyText:
        Version: "2012-10-17"
        Statement: 
          - Sid: AllowPushPull
            Effect: Allow
            Principal:
              AWS:
                - !Ref 'AWS::AccountId'
            Action:
              - "ecr:GetDownloadUrlForLayer"
              - "ecr:BatchGetImage"
              - "ecr:BatchCheckLayerAvailability"
              - "ecr:PutImage"
              - "ecr:InitiateLayerUpload"
              - "ecr:UploadLayerPart"
              - "ecr:CompleteLayerUpload"

  # ------------
  # ECS Cluster
  # ------------
  JenkinsLabECSCluster:
    Type: "AWS::ECS::Cluster"
    Properties:
      ClusterName: "Jenkins-SimpleAPI"


  # ================================
  # Fargate Task Definition
  # ================================
  JenkinsSlaveTaskDefinition: 
    Type: "AWS::ECS::TaskDefinition"
    Properties: 
      RequiresCompatibilities:
        - "EC2"
        - "FARGATE"
      ExecutionRoleArn: !Ref ECSTaskExecutionRole
      TaskRoleArn: !Ref ECSTaskExecutionRole
      NetworkMode: "awsvpc"
      Cpu: "512"
      Memory: "1024"
      ContainerDefinitions: 
        - Name: 'SimpleAPI'
          Image: !Sub ${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${JenkinsLabECRRepository}:latest
          PortMappings: 
          - ContainerPort: 80
            HostPort: 80
          Essential: "true"

  # ECS Task Role
  ECSTaskExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Effect: Allow
          Principal:
            Service: [ecs-tasks.amazonaws.com]
          Action: ['sts:AssumeRole']
      Path: /
      Policies:
        - PolicyName: "ECSTaskExecutionPolicy"
          PolicyDocument:
            Statement:
            - Effect: Allow
              Action:
                # Allow the ECS Tasks to download images from ECR
                - 'ecr:GetAuthorizationToken'
                - 'ecr:BatchCheckLayerAvailability'
                - 'ecr:GetDownloadUrlForLayer'
                - 'ecr:BatchGetImage'

                # Allow the ECS tasks to upload logs to CloudWatch
                - 'logs:CreateLogStream'
                - 'logs:PutLogEvents'
              Resource: '*'

#===================================
# Outputs:
# Specify any outputs for the stack.
#===================================
Outputs:
  # Build Artifact Bucket
  JenkinsLabBucketName:
    Description: Name of the S3 bucket used for Lab Build Artifacts.
    Value: !Ref JenkinsLabBucket
  
  # ECR Repository
  JenkinsLabECRRepositoryName:
    Description: "The Name of the ECR Repository created to hold the SimpleAPI Image"
    Value: !Ref JenkinsLabECRRepository

  JenkinsLabECRRepositoryArn:
    Description: "The ARN of the ECR Repository created to hold the SimpleAPI Image"
    Value: !GetAtt 'JenkinsLabECRRepository.Arn'

  JenkinsLabECRRepositoryRepo:
    Description: "The Repository path location or address that will be used to build and push the SimpleAPI Image"
    Value: !Sub ${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${JenkinsLabECRRepository}:latest

  # ECS Cluster
  JenkinsLabECSClusterName:
    Description: "The Name of the empty Fargate cluster created to run Jenkins deployments."
    Value: !Ref 'JenkinsLabECSCluster'

  JenkinsLabECSClusterArn:
    Description: "The ARN of the empty Fargate cluster created to run Jenkins deployments."
    Value: !GetAtt 'JenkinsLabECSCluster.Arn'

  # ECS Role
  ECSTaskExecutionRoleName:
    Description: The NAME of the ECS Fargate Task Role
    Value: !Ref 'ECSTaskExecutionRole'

  ECSTaskExecutionRoleARN:
    Description: The ARN of the ECS Fargate Task Role
    Value: !GetAtt 'ECSTaskExecutionRole.Arn'